// Setup the dependencies

const express = require("express");
const mongoose = require("mongoose");
const dotenv = require("dotenv").config();


const taskRoute = require("./routes/taskRoute")

// Server setup
const app = express();
const port = 7000;
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Database Connection
// Connect to MongoDB Atlas 
//MRC means Model Routes Control
// useUnifiedTopology means pang stable ng connection.
// between mongodb.net/ and ?retry just put MRC to run your code in POSTMAN software.
mongoose.connect(`mongodb+srv://juvix45:${process.env.PASSWORD}@cluster0.67sy6wx.mongodb.net/MRC?retryWrites=true&w=majority`,
	{
		useNewUrlParser : true,
		useUnifiedTopology : true
	}



);

// ${process.env.PASSWORD}

// ===============================================================================================

// Setup noticification for connection success or failure

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.on('open', () => console.log('Connected to MongoDB'));

// Add the task routes
// Allows all the task routes created in the "tashRoutes.js" files to use " " route
// "/task"

app.use("/tasks", taskRoute);


// Server listening // Port connect
app.listen(port, () => console.log(`Now listening to port ${port}`));

