// Contains all the endpoint for our application

// We need to use express' Router() function to achieve
// const { response } = require("express");
const express = require("express");

// Creates a Router instance that function as a middleware and routing system.
// Allows access to HTTP method middlewares that makes it easier to create routes for our application.
// ../controller/taskController means yung doubledot is parent folder tapos controller tapos taskController.
const router = express.Router();

// import
const taskController = require("../controller/taskController");


/*
    Syntax : localhost:3002/tasks/getinfo

*/



// [SECTION] Routes
// instead of typing const router = express.Router(); We shortcut it into router.get("/")
// request & response

// Route to get all the task 
router.get("/getinfo", (req, res) => {

    taskController.getAllTasks().then(
        resultFromController => res.send(resultFromController));

})

// Route to create a new task
router.post("/create", (req, res) => {

    // If information will be coming from client side commonly from forms, the data can be accessed from the request "body" property.
    taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
})

// Route to delete a task
// The colon (:) is an identifier that helps create a dynamic route which allows us o supply information in the URL
// The word that comes after the colon (:) symbol will be the name of the URL parameter.
// ":id" is a wildcard where you can put any value, it then creates a link between "id" parameter in the URL and the value provided in the URL

/*
    Example:
    localhost:6000/tasks/delete/63c67b14f1f503c5f8bd9bc4
    The 1234 is assigned to the "id" parameter in the URL


// This 63c67b14f1f503c5f8bd9bc4 is came from POSTMAN and we need to paste it in router.delete("/delete/:id", (req,res) => {

    taskController.
})
*/
// params is doon sa postman na PARAMS
router.delete("/delete/:id", (req,res) => {

    // URL parameter values are accessed via the request object's "params" property
	// The property name of this object will match the given URL parameter name
	// In this case "id" is the name of the parameter
	// If information will be coming from the URL, the data can be accessed from the request "params" property
    taskController.deleteTask(req.params.id).then(result => res.send(result));
})

// Route to update a task
router.put("/update/:id", (req, res) => {

    taskController.updateTask(req.params.id, req.body).then(result => res.send(result));
})

// =================ACTIVITY-START==============================================
// 1.
router.put("/updateStatus/:id", (req, res) => {

    taskController.updateStatus(req.params.id ,req.body).then(resultFromController => res.send(resultFromController));
});

// 2.
router.get("/:id", (req, res) => {
    const id = req.params.id;
    taskController.getTask(id).then(resultFromController => res.send(resultFromController));
});



// ==============END-of-ACTIVITY==============================================

// Use "module.exports" to export the router object to use in the "app.js"
// export
module.exports = router;